module AVL
  class Base
    def initialize
      @root = nil
    end

    def insert(data)
      parent = nil
      if !@root
        parent = Node.new(data, nil)
        @root = parent
      else
        parent = @root.insert(data, @root)
      end
      rebalance(parent)
    end

    def rotate_right(node)
      tmp = node.left
      tmp.parent = node.parent

      node.left = tmp.right

      if node.left
        node.left.parent = node
      end

      tmp.right = node
      node.parent = tmp

      if tmp.parent
        if tmp.parent.right == node
          tmp.parent.right = tmp
        else
          tmp.parent.left = b
        end
      end

      self.set_balance(node)
      self.set_balance(tmp)

      return tmp
    end

    def rotate_right_left(node)
      node.right = self.rotate_right(node.right)
      return self.rotate_left(node)
    end

    def rotate_left(node)
      tmp = node.right
      tmp.parent = node.parent
      node.right = tmp.left

      if node.right
        node.right.parent = node
      end

      tmp.left = node
      node.parent = tmp

      if tmp.parent
        if tmp.parent.right == node
          tmp.parent.right = tmp
        else
          tmp.parent.left = tmp
        end
      end
      self.set_balance(node)
      self.set_balance(tmp)

      return tmp
    end

    def rotate_left_right(node)
      node.left = self.rotate_left(node.left)
      return self.rotate_right(node)
    end

    def rebalance(parent)
      self.set_balance(parent)

      if parent.balance < -1
        if self.height(parent.left.left) >= self.height(parent.left.right)
          parent = self.rotate_right(parent)
        else
          parent = self.rotate_left_right(parent)
        end
      elsif parent.balance > 1
        if self.height(parent.right.right) >= self.height(parent.right.left)
          parent = self.rotate_left(parent)
        else
          parent = self.rotate_right_left(parent)
        end
      end

      if parent.parent
        self.rebalance(parent.parent)
      else
        @root = parent
      end
    end

    def set_balance(node)
      node.balance = self.height(node.right) - self.height(node.left)
    end

    def height(node)
      if !node
        return -1
      else
        return 1 + [self.height(node.left), self.height(node.right)].max
      end
    end

    def traverse_in_order
      @root.traverse_in_order.strip if @root
    end
  end
end
