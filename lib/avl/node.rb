module AVL
  class Node
    attr_accessor :left, :right, :parent, :data, :balance

    def initialize(data, parent)
      @parent, @data, right, right, @balance = parent, data, nil, nil, 0
    end

    def insert(data, parent)
      if (data < @data)
        if !self.left
          self.left = Node.new(data, parent)
        else
          self.left.insert(data, parent)
        end
      elsif (data > @data)
        if !self.right
          self.right = Node.new(data, parent)
        else
          self.right.insert(data, parent)
        end
      end
      return parent
    end

    def get_max
      if self.right
        return self.right.get_max
      else
        return self.data
      end
    end

    def get_min
      if self.left
        return self.left.get_min
      else
        return self.data
      end
    end

    def traverse_in_order
      s = ""
      s += left.traverse_in_order if left
      s += "#{@data} "
      s += right.traverse_in_order if right
      s
    end
  end
end
