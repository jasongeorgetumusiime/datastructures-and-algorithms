class Queue
  def initialize;    @store = [];         end
  def enqueue(n)
    if(n.is_a?(Array))
      @store.concat n
    else
      @store << n
    end
  end
  def dequeue;       @store.shift;        end
  def peek;          @store.first;        end
  def length;        @store.length;       end
  def empty?;        @store.empty?;       end
end