module LinkedList
  class Node
    attr_accessor :next
    attr_reader :data

    def initialize(data)
      @data = data
      @next = nil
    end

    def remove(data, prev)
      if @data == data
        prev.next = self.next
      else
        @next.remove(data, self) if self.next
      end
    end
  end
end
