module LinkedList
  class Base
    attr_reader :size

    def initialize
      @head = nil
      @size = 0
    end

    def insertStart(data)
      newNode = LinkedList::Node.new(data)
      newNode.next = @head if @head
      @head = newNode
      @size += 1
    end

    def insert(data, at: :start)
      insertEnd(data) if at == :end
      insertStart(data) if at == :start
    end

    def insertEnd(data)
      node = LinkedList::Node.new(data)
      last = @head
      while last.next
        last = last.next
      end
      last.next = node
    end

    def remove(data)
      if @head
        if data == @head.data
          @head = @head.next
        else
          @head.remove(data, @head)
        end
      end
    end

    # Challenge: Can you make he Linked List Enumerable, such that it uses `.each`
    def to_s
      node = @head; s = ""
      while node
        s += "#{node.data} "
        node = node.next
      end
      s.strip
    end
  end
end
