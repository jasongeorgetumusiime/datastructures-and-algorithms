module Search
  extend self

  def binary(list, item)
    lower_bound = 0
    upper_bound = list.length - 1

    until lower_bound > upper_bound
      middle = (lower_bound + upper_bound) / 2
      attempt = list[middle]
      if attempt == item
        return attempt
      elsif attempt > item
        upper_bound = middle - 1
      else
        lower_bound = middle + 1
      end
    end
  end
end
