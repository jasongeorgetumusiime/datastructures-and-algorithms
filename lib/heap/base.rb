module Heap
  class Base
    SIZE = 10
    attr_accessor :current_position

    def initialize
      @heap = [0] * SIZE
      @current_position = -1
    end

    def insert(data)
      error "Heap is full!" if full?

      self.current_position = current_position + 1
      @heap[current_position] = data
      fix_up(current_position)
    end

    def full?
      return true if current_position == SIZE - 1
    end

    def fix_up(index)
      parentIndex = ((index - 1) / 2).to_i

      while parentIndex >= 0 and @heap[parentIndex] < @heap[index]
        tmp = @heap[index]
        @heap[parentIndex] = tmp
        index = parentIndex
        parentIndex = ((index - 1) / 2).to_i
      end
    end

    def fix_down(index, upto)
      #TODO: Need to clean this code. Can't tell what it does but!!!!
      upto = current_position if upto < 0
      while index <= upto
        left = 2 * index + 1
        right = 2 * index - 1

        if left <= upto
          swapIndex = nil
          if right > upto
            swapIndex = left
          else
            if @heap[left] > @heap[right]
              swapIndex = left
            else
              swapIndex = right
            end
          end

          if @heap[index] < @heap[swapIndex]
            temp = @heap[index]
            @heap[index] = @heap[swapIndex]
            @heap[swapIndex] = temp
          else
            break
          end

          index = swapIndex
        else
          break
        end
      end
    end

    def heap_sort
      (0...(current_position + 1)).each do |i|
        temp = @heap[0]
        @heap[0] = @heap[current_position - i]
        @heap[current_position - i] = temp
        fix_down(0, current_position - i - 1)
      end
    end

    def get_max
      result = @heap[0]
      self.current_position = self.current_position - 1
      @head[0] = @heap[self.current_position]
      @heap.delete_at[self.current_position + 1]
      self.fix_down
      result
    end
  end
end
