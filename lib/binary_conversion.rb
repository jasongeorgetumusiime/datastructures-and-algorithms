#!/bin/ruby

def decimal_to_binary(n)
  bin = []
  
  return "0" if n == 0
  until n == 0
      rem = n % 2
      n = n / 2
      bin.push(rem)
  end
  
  bin.join()
end