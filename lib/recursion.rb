module Recursion
  extend self
  # In real projects you would never have to write methods as ruby has optimized and
  # standardized idioms of solving the following problems

  #Also:
  # => numbers.sum()
  # => numbers.inject(:+)
  def sum_numbers(numbers)
    return 0 if (numbers.empty?)
    return numbers[0] + sum_numbers(numbers.drop(1))
  end

  ##Also:
  # => numbers.inject {|a, b| a > b ? a : b}
  def find_max(numbers)
    find_max_acc = lambda do |max, arr|
      return max if arr.empty?
      return find_max_acc.call(max < arr[0] ? arr[0] : max, arr.drop(1))
    end
    find_max_acc.call(numbers.first, numbers.drop(1))
  end

  ##Also:
  # => numbers.count
  # => numbers.length
  def count(numbers)
    if numbers.empty?; 0 else 1 + count(numbers.drop(1)); end
  end

  def fib(n)
    if n <= 1
      return n, 0
    else
      a, b = self.fib(n - 1)
      return a + b, a
    end
  end

  def disk_usage(path)
    # https://stackoverflow.com/questions/17354864/ruby-filter-array-by-regex
    puts "#{file_size = File.size(path)} #{path}"
    if File.directory?(path)
      file_size + Dir.glob("#{path}/*", File::FNM_DOTMATCH)
        .reject { |f| f =~ /\/\.{1,2}$/i }
        .sum { |f| self.disk_usage(f) }
    else
      file_size
    end
  end

  def factorial(n)
    n == 0 ? 1 : n * self.factorial(n - 1)
  end

  def quicksort(numbers)
    if (numbers.length < 2)
      numbers
    else
      pivot, lesser, greater = numbers.length / 2, [], []
      numbers.each.with_index do |n, index|
        unless index == pivot
          if n <= numbers[pivot]; lesser << n; else greater << n; end
        end
      end
      quicksort(lesser) + [numbers[pivot]] + quicksort(greater)
    end
  end

  def selection_sort(arr)
    sorted_arr = []
    (0...arr.length).each do |v|
      smallest_index = find_smallest_index(arr)
      sorted_arr << arr.delete_at(smallest_index)
    end
    sorted_arr
  end

  def power(x, n)
    if n == 0
      1
    else
      tmp = self.power(x, n / 2)
      result = tmp * tmp
      n % 2 == 0 ? result : result *= x
    end
  end

  module EnglishRuler
    extend self

    def draw_line(tick_length, tick_label = "")
      line = "-" * tick_length
      line += " " + tick_label
      puts line
    end

    def draw_interval(center_length)
      if center_length > 0
        self.draw_interval(center_length - 1)
        self.draw_line(center_length)
        self.draw_interval(center_length - 1)
      end
    end

    def draw_ruler(num_inches, major_length)
      self.draw_line(major_length, "0")
      (1...(1 + num_inches)).each do |j|
        self.draw_interval(major_length - 1)
        self.draw_line(major_length, j.to_s)
      end
    end
  end

  private

  def find_smallest_index(arr)
    smallest_index = 0
    smallest = arr[smallest_index]
    arr.each.with_index do |v, i|
      smallest, smallest_index = v, i if v < smallest
    end
    smallest_index
  end
end
