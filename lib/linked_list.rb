module LinkedList
  autoload :Base, "linked_list/base"
  autoload :Node, "linked_list/node"
end
