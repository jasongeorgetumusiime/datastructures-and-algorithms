require 'byebug'
require_relative 'queue'

#TODO: Missing unit test
graph          = {}
graph[:you]    = [:alice, :bob, :claire]
graph[:bob]    = [:anuj, :peggy]
graph[:alice]  = [:peggy]
graph[:claire] = [:thom, :jonny]
graph[:anuj]   = []
graph[:peggy]  = [:martha]
graph[:thom]   = []
graph[:jonny]  = []
graph[:martha] = [:you]

def bfs(graph, start, &pred)
  return person if pred.call(start)
  search_queue = Queue.new
  search_queue.enqueue(graph[start])
  visited = [] << start
  until search_queue.empty?
    person = search_queue.dequeue()
    unless person == nil || (visited.include? person)
      return person if pred.call(person)
      search_queue.enqueue(graph[person])
      visited << person
    end
  end
end