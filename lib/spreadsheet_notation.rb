module SpreadsheetNotiation

  #Finds the spread notation of the form A1..ZZ1, A2...ZZ2, etc
  # given a position n on the spreadsheet within [1,10^9]

  def self.row_length
    702
  end

  def self.max_row_idx
    10 ** 9
  end

  def self.letter_count
    26
  end

  def self.max_idx
    10 ** 12
  end

  def self.lex_start_index
    64
  end

  def self.convert(n)
    return if !(1..max_idx).include?(n)
    "#{row_label(n)}#{col_label(n)}"
  end

  def self.row_label(n)
    row_idx = n / row_length

    raise Exception ("Row index cannot go beyond 10^9") if row_idx > max_row_idx

    if (n % row_length == 0)
      return row_idx
    else
      return (row_idx + 1)
    end
  end

  def self.col_label(n)
    col_idx = n % row_length
    col_idx = row_length if (col_idx == 0 && n > 0)

    lex_idx_1 = col_idx / letter_count #    lexical index of label 1
    lex_idx_2 = col_idx % letter_count #    lexical index of label 2

    if (lex_idx_2 == 0 && n > 0)
      # - Position is at the end and not beginning
      #   of the next column notation in the sequence (Z, AZ, BZ...).
      # - Consider the first column label to be nil
      lex_idx_2 = letter_count
      lex_idx_1 -= 1
    end

    first_col_label = upcase_alphabet(lex_idx_1)
    second_col_label = upcase_alphabet(lex_idx_2)
    "#{first_col_label}#{second_col_label}"
  end

  def self.upcase_alphabet(i)
    # Convert i to Uppercase Alphabet letters starting with A at 1
    # and ending with Z at 26. Returns nil otherwise
    return (i + lex_start_index).chr("UTF-8") if (1..letter_count).include?(i)
  end
end
