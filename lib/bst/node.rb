module BST
  class Node
    attr_accessor :right, :left, :data

    def initialize(data)
      @data, @left, @right = data, nil, nil
    end

    def insert(data)
      if data < @data
        if !left
          self.left = Node.new(data)
        else
          left.insert(data)
        end
      else
        if !right
          self.right = Node.new(data)
        else
          right.insert(data)
        end
      end
    end

    def get_min
      if !left
        @data
      else
        left.get_min
      end
    end

    def get_max
      if !right
        @data
      else
        right.get_max
      end
    end

    def remove(data, parent)
      if data < @data
        self.left.remove(data, self)
      elsif data > @data
        self.right.remove(data, self)
      else
        if right and left
          @data = right.get_min
          right.remove(@data, self)
        elsif parent.right == self
          if self.left
            parent.right = self.left
          else
            parent.right = self.right
          end
        elsif parent.left == self
          if self.left
            parent.left = self.left
          else
            parent.left = self.right
          end
        end
      end
    end

    def to_s
      s = ""
      s += left.to_s if left
      s += "#{@data} "
      s += right.to_s if right
      s
    end
  end
end
