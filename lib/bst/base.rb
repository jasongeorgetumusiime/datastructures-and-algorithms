module BST
  class Base
    def initialize
      @root = nil
    end

    def insert(data)
      if @root
        @root.insert(data)
      else
        @root = Node.new(data)
      end
    end

    def remove(data)
      if @root
        if @root.data == data
          temp = Node.new(nil)
          temp.left = @root
          @root.remove(data, temp)
        else
          @root.remove(data, nil)
        end
      end
    end

    def get_max
      @root.get_max if @root
    end

    def get_min
      @root.get_min if @root
    end

    def to_s
      @root.to_s.strip if @root
    end
  end
end
