require "spreadsheet_notation.rb"

describe "spreadsheet_notation" do
  it "converts row cell index [1,10^9] to spreadsheet notation" do
    expect(SpreadsheetNotiation.convert(0)).to be_nil
    expect(SpreadsheetNotiation.convert(1)).to eq "1A"
    expect(SpreadsheetNotiation.convert(26)).to eq "1Z"
    expect(SpreadsheetNotiation.convert(27)).to eq "1AA"
    expect(SpreadsheetNotiation.convert(702)).to eq "1ZZ"
    expect(SpreadsheetNotiation.convert(703)).to eq "2A"
    expect(SpreadsheetNotiation.convert(1404)).to eq "2ZZ"
    expect(SpreadsheetNotiation.convert(1405)).to eq "3A"
    expect(SpreadsheetNotiation.convert(10 ** 13)).to be_nil
  end
end
