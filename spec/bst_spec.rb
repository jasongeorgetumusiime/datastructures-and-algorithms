require "bst"

describe BST do
  let(:bst) { BST::Base.new }
  before(:example) do
    bst.insert(10)
    bst.insert(-2)
    bst.insert(01)
    bst.insert(12)
    bst.insert(07)
    bst.insert(03)
  end
  it "can have data inserted into it" do
    bst.insert(60)
    expect(bst.to_s).to eq "-2 1 3 7 10 12 60"
  end

  it "can have data deleted from it" do
    bst.remove(10)
    expect(bst.to_s).to eq "-2 1 3 7 12"
  end

  it "can determine the min value in the tree" do
    expect(bst.get_min).to eq(-2)
  end

  it "can determine the max value in the tree" do
    expect(bst.get_max).to eq(12)
  end
end
