require "search"

describe "binary_search" do
  it "looks up an item in a sorted list with O(log(n)) runtime" do
    expect(Search.binary([1, 3, 5, 7, 9], 9)).to eq 9
    expect(Search.binary([1, 3, 5, 7, 9], 0)).to eq nil
    expect(Search.binary(["Anny", "Bob", "Eddy", "Fletcher", "Zoey"], "Bob")).to eq "Bob"
  end
end
