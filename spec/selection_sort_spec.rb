require 'recursion'

describe "#selection_sort" do
  it "sorts a list of items by searching looking up the current smallest item" do
    expect(Recursion.selection_sort([1, 8, 9, 7, 3, 0])).to eq [0, 1, 3, 7, 8, 9]
  end
end