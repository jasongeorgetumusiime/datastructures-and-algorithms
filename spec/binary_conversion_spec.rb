require 'binary_conversion'

describe "Binary conversion" do
  it "converts decimal numbers to binary strings" do
    expect(decimal_to_binary(2)).to eq "01"
    expect(decimal_to_binary(0)).to eq "0"
  end
end