require "linked_list"

describe LinkedList do
  let(:list) { LinkedList::Base.new }

  it "adds an item at the beginning" do
    list.insert(1)
    expect(list.to_s).to eq("1")
  end

  it "adds an item at the end of the list" do
    list.insert(1)
    list.insert(2, at: :end)
    expect(list.to_s).to eq("1 2")
  end

  it "removes an item at with O(N) complexity" do
    list.insert(4)
    list.insert(3)
    list.insert(2)
    list.insert(1)
    list.remove(4)
    expect(list.to_s).to eq("1 2 3")
  end
end
