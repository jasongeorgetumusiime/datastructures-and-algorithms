require "heap"

describe Heap do
  let(:heap) { Heap::Base.new() }
  before (:example) do
    heap.insert(12)
    heap.insert(-3)
    heap.insert(23)
    heap.insert(4)
  end

  it "implements heapsort algorithm", skip: true do
    #TODO: Heap sort is lacking
    expect(heap.heap_sort()).to eq("23 12 4 -3")
  end
end
