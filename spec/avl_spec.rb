require "avl"

describe AVL do
  let(:avl) { AVL::Base.new() }
  it "can rebalance right heavy binary tree on insert" do
    avl.insert(4)
    avl.insert(6)
    avl.insert(5)
    expect(avl.traverse_in_order).to eq("4 5 6")
  end

  it "can rebalance left heavy tree on insert" do
    avl.insert(4)
    avl.insert(2)
    avl.insert(3)
    expect(avl.traverse_in_order).to eq("2 3 4")
  end
end
