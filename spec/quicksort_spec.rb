require 'recursion'

describe "#quicksort" do
  it "re arranges a list in ascending order using dynamic programming technique" do
    expect(Recursion.quicksort([10, 5, 2, 3])).to eq [2, 3, 5, 10]
  end
end