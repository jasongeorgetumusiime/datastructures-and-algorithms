require "recursion"

describe "#recursion" do
  it "find to find the sum of n mumbers" do
    expect(Recursion.sum_numbers([1, 2, 3, 4, 5, 6, 7, 8, 9])).to eq 45
  end

  it "to find the biggest of n mumbers" do
    expect(Recursion.find_max([1, 2, 3, 4, 5, 6, 7, 8, 9])).to eq 9
  end

  it "to count number of items in a list" do
    expect(Recursion.count([1, 2, 3, 4, 5, 6, 7, 8, 9, 0])).to eq 10
  end
end
